Spring MVC example project
=========================================

Summary
-------
A Twitter-like application based on Spring called Spitter.
Based on Spring in Action, Third Edition by Craig Walls.